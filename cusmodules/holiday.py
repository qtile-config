#!/usr/bin/env python

#Copyright (C) 2024  Michael Kantor

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
from subprocess import run
from datetime import datetime
from os.path import expanduser
from calendar import monthrange

def getHoliday(month, day):
    if month == 1 and day == 1:
        return 'none'
        #return "new_year"

    #Arch birthday
    elif month == 3 and day == 11:
        return 'none'
        #return 'arch_birthday'

    #April fools
    elif month == 4 and day == 1:
        return 'none'
        #return 'april_fools'

    #Halloween
    elif month == 10:
        return 'halloween'

    #Thanksgiving
    elif month == 11 and datetime.now().weekday() + 20 == day:
        return 'thanksgiving'

    #Christmas
    elif month == 12:
        return 'xmas'

    else:
        return 'none'

def main():
    home = expanduser('~')
    month = datetime.now().month
    day = datetime.now().day
    holiday = getHoliday(month, day)

    return holiday

if __name__ == "__main__":
    print(main())

    sys.exit(0)
