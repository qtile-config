#!/usr/bin/env python

#Copyright (C) 2024  Michael Kantor

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import json
import wallpaper
from pathlib import Path
from random import randint
from subprocess import run

def random(colors):
    colorcount = len(colors) - 1
    ranint = randint(0, colorcount)
    rancol = colors[ranint]

    return rancol

def setColor(home, color_dir, choice, **kwargs):
    os.system(f'wal --theme {color_dir}/{choice}')

    with open(f"{home}/.cache/wal/colors.json", 'r') as f:
        data = json.load(f)

        top_color = data["colors"].get('color8')

        bottom_color = data["colors"].get('color6')

    wallpaper.gen_pylogo(top_color, bottom_color)

    try:
        if kwargs["norestart"] == True:
            return 0

    except:
        pass

    #run(['qtile', 'shell', '-c', 'restart()'])
    #run(['kill', '-SIGUSR1', os.popen('pidof qtile').read().strip('\n')])
    run([f'{home}/git/qtile/bin/qtile', 'shell', '-c', 'reload_config()'])
    run([f'{home}/.local/bin/pywalfox', 'update'])

    return 0

def main(argc, argv):
    home = str(Path.home())
    color_dir = f'{home}/.config/qtile/colors'

    try:
        if argv[0] == '-c':
            choice = argv[1]

        else:
            choice = os.popen(f'ls {color_dir} | dmenu -p "Color:"').read().strip("\n")

    except:
        choice = os.popen(f'ls {color_dir} | dmenu -p "Color:"').read().strip("\n")


    if "" == choice:
        return 0
    
    elif "random" in choice:
        colors = os.popen(f'ls {color_dir}').read().strip('\n').split('\n')
        choice = random(colors)

    else:
        pass

    setColor(home, color_dir, choice)

    return 0

if __name__ == '__main__': 
    sys.argv.pop(0)
    argc = int(len(sys.argv))

    try:
        argv = sys.argv

    except:
        argv = []

    main(argc, argv)
