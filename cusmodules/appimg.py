#!/usr/bin/env python

#Copyright (C) 2024  Michael Kantor

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import re
from wallpaper import  dmenu

def main():
    home = os.path.expanduser('~')
    appdir = home + '/Applications/'
    apps = os.popen(f"ls {appdir}").read().strip('\n')
    appslist = apps.split('\n')
    appsec = re.sub('[0-9]|x86|_|\.|\-|AppImage', '', apps)
    app = dmenu(appsec, "Apps:")

    if app == '':
        return 1

    for i in appslist:
        try:
            fnm = re.search(app, i).span()
            print(fnm)
            id_ = appslist.index(i)
            print(id_)

        except Exception as e:
            print(e)
            pass

    appnm = appslist[id_]
    os.system(appdir + appnm)

if __name__ == '__main__':
    main()

