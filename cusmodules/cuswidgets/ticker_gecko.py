#!/usr/bin/env python

#Copyright (C) 2024  Michael Kantor

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import json
import time
import requests
import subprocess
from datetime import datetime
from threading import Thread

def match(currency):
    currencies = {
        "BTC": "bitcoin",
        "ETH": "ethereum",
        "XRP": "xrp",
        "LTC": "litecoin",
        "BCH": "bitcoin-cash",
        "XMR": "monero",
        "DOGE": "dogecoin"
    }

    return currencies[currency]

def Gecko(currs):
    #prices = '' 
    prices = {}

    for i in currs:
        curr = match(i)

        source = requests.get(f'https://api.coingecko.com/api/v3/coins/{curr}/').text

        data = json.loads(source)

        price = str(data['market_data']['current_price']['usd'])

        try:
            if len(price[:price.index('.')]) > 3:
                price = price[:2] + ',' + price[2:]

        except:
            price = price[:2] + ',' + price[2:]

        price = '$' + price

        price = {i: price}

        prices.update(price)

        with open('/tmp/crprices.json', 'w') as f:
            f.write(str(prices).replace("'", '"'))

def main():
    global crpos
    home = os.path.expanduser('~')
    currs = []
    priced = ''

    try:
        crpos += 1

    except:
        crpos = 0

    try:
        with open('/tmp/crprices.json', 'r') as f:
            prices_str = f.read()
            prices = json.loads(prices_str)

        for i in prices:
            currs += i

        for i in prices:
            if currs[len(currs) - 1] == i:
                price = f'{i}: {prices[i]} |'

            else:
                price = f'{i}: {prices[i]} | '
        
            priced += price

        leng = len(priced)

        #print(crpos >= leng)
        if crpos >= leng:
            crpos = 0

        crprices = f"{priced[crpos:]} {priced[:crpos]}"
        
        #print(priced)
        #print(crpos)
        #print(leng)
        #print(crprices)
        #print(crprices[:15], '\n')
        return crprices[:15]

    except Exception as e:
        with open(f'{home}/.local/share/ticker_gecko/ticker_gecko.log', 'w') as f:
            f.write(e)

        return f'ERROR! Check "{home}/.local/share/ticker_gecko/ticker_gecko.log".'

if __name__ == '__main__':
    if sys.argv[1] == '--help' or sys.argv[1] == '-help' or sys.argv[1] == '--h' or sys.argv[1] == '-h':
        print('Usage:\n\tStart server by running command: "/path/to/ticker_gecko.py".\n\tPut abbreviations of crypto in arguments: "/path/to/ticker_gecko.py BTC LTC" (will get price for bitcoin and litecoin)\n\tAdd main function to GenPollText widget in config.py: "\n\t\timport ticker_gecko\n\t\twidget.GenPollText(\n\t\t\tfunc=ticker_gecko.main,\n\t\t\tupdate_interval= 0.5),"')
        sys.exit(0)

    currs = []
    argc = len(sys.argv)
    for i in range(argc):
        if i == 0:
            pass

        else:
            currs.append(sys.argv[i])

    while True:
        Gecko(currs)

        with open('/tmp/crprices.json', 'r') as f:
            leng = len(f.read())
        
        repeat_time = leng - ((len(currs) - 1) * 5)

        time.sleep(repeat_time)
        
