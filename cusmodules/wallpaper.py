#!/usr/bin/env python

#Copyright (C) 2024  Michael Kantor

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import json
import toml
import signal
from PIL import Image
from pathlib import Path
from random import randint
from subprocess import run

import colors

def dmenu(input_, prompt):
    result = os.popen(f'echo "{input_}" | dmenu -p "{prompt}"').read().strip("\n")

    return result

def gen_pylogo(top_color, bottom_color):
    home = str(Path.home())
    run(['convert', f'{home}/.config/qtile/topsnake.png', '+level-colors', f"gray,'{top_color}'", '-transparent', 'gray', '/tmp/topsnake.png'])

    run(['convert', f'{home}/.config/qtile/bottomsnake.png', '+level-colors', f"gray,'{bottom_color}'", '-transparent', 'gray', '/tmp/bottomsnake.png'])

    run(['convert', '-size', '600x600', '-composite', '/tmp/bottomsnake.png', '/tmp/topsnake.png', '/tmp/python.png'])

    #run(['convert', '-size', '610x610', '-composite', f'{home}/.config/qtile/border.png', '/tmp/python.png', '/tmp/python.png'])

    run(['rm', '-r', '/tmp/topsnake.png', '/tmp/bottomsnake.png'])

def preset(wall, mode):
    global home
    presets = os.popen(f"ls {home}/Pictures/Wallpapers/presets").read().strip('\n').split('\n')
    wall = wall[:-3]
    wall += 'json'

    if mode == 'find':
        return wall in presets

    elif mode == 'set':
        wall = presets.index(wall)
        return presets[wall]

def new(name, folder):
    os.system(f"ln -s {home}/Pictures/Wallpapers/wallpapers/{name} {home}/Pictures/Wallpapers/wallpaperdirs/{folder}/{name}")

def pywal(wall, loc):
    global home

    if wall == 'archstylized.png':
        nitro(wall, loc)

        os.system(f'wal --theme {home}/.config/qtile/colors/defaultcolors.json')

        return 0


    else:
        pass

    os.system(f'wal -i {loc}/{wall}')

def nitro(wall, loc):
    global home
    os.system(f'nitrogen --set-scaled {loc}/{wall}')

def random(walls):
    wallcount = len(walls) - 1
    ranint = randint(0, wallcount)
    ranwall = walls[ranint]
    
    return ranwall

if __name__ == '__main__':
    #Set dir variables
    home = str(Path.home())
    stdwalls = f'{home}/Pictures/Wallpapers/wallpapers'
    hallo = f'{home}/Pictures/Wallpapers/halloweenwallpapers'
    xmas = f'{home}/Pictures/Wallpapers/xmaswallpapers'

    if len(sys.argv) >= 2:
        if sys.argv[1] == 'none':
            wallpapers = os.popen(f'ls {stdwalls}').read().strip('\n').split('\n')
            wall = random(wallpapers)

            if preset(wall, 'find'):
                os.system(f'wal --theme {home}/Pictures/Wallpapers/presets/{preset(wall, "set")}')
                run([f'{home}/.local/bin/pywalfox', 'update'])
        
            else:

                pywal(wall, stdwalls)

            with open(f"{home}/.cache/wal/colors.json", 'r') as f:
                data = json.load(f)

                top_color = data["colors"].get('color8')

                bottom_color = data["colors"].get('color6')

            gen_pylogo(top_color, bottom_color)

        elif sys.argv[1] == 'halloween':
            curr_dir = hallo

            wallpapers = os.popen(f'ls {curr_dir}').read().strip('\n').split('\n')
    
            walls = random(wallpapers)

            nitro(walls, curr_dir)

            colors.setColor(home, f'{home}/.config/qtile/colors', 'halloweencolors.json', norestart = True)

            run([f'{home}/.local/bin/pywalfox', 'update'])

            sys.exit(0)

        elif sys.argv[1] == 'xmas':
            curr_dir = xmas

            wallpapers = os.popen(f'ls {curr_dir}').read().strip('\n').split('\n')
    
            walls = random(wallpapers)

            nitro(walls, curr_dir)

            colors.setColor(home, f'{home}/.config/qtile/colors', 'xmascolors.json', norestart = True)

            run([f'{home}/.local/bin/pywalfox', 'update'])

            sys.exit(0)

        elif sys.argv[1] == 'new':
            new(sys.argv[2], sys.argv[3])

        elif '-i' == sys.argv[1]:
            if sys.argv[2][0] == '/':
                indx = sys.argv[2].rindex('/')
                curr_dir  = sys.argv[2][:indx]
                wall = sys.argv[2][indx + 1:]

            else:
                curr_dir = os.getcwd()
                wall = sys.argv[2]

            pywal(wall, curr_dir)

            run([f'{home}/.local/bin/pywalfox', 'update'])

            with open(f"{home}/.cache/wal/colors.json", 'r') as f:
                data = json.load(f)

                top_color = data["colors"].get('color8')

                bottom_color = data["colors"].get('color6')

            gen_pylogo(top_color, bottom_color)

            try:
                #subprocess.check_call(['qtile', 'shell', '-c', 'restart()'])
                #run(['kill', '-SIGUSR1', os.popen('pidof qtile').read().strip('\n')])
                #run(['qtile', 'shell', '-c', 'restart()'])
                run([f'{home}/git/qtile/bin/qtile', 'shell', '-c', 'reload_config()'])

            except:
                os.system('killall dwm')

        elif 'a' in sys.argv[1]:
            curr_dir = sys.argv[1] + 'wallpapers'

            wallpapers = os.popen(f'ls {home}/Pictures/Wallpapers/{curr_dir}').read().strip('\n').split('\n')
            nitro(random(wallpapers), curr_dir)

        sys.exit(0)

    else:
        pass

    #Inital prompt
    walldirs = "All\n"
    walldirs += os.popen(f"ls {home}/Pictures/Wallpapers/wallpaperdirs/").read().strip("\n")
    #wallpaperchoice = os.popen(f'echo "{walldirs}" | dmenu -p "Wallpaper:"').read().strip("\n")
    wallpaperchoice = dmenu(walldirs, "Wallpaper:")

    #Closes on ESC
    if wallpaperchoice == '':
        sys.exit(0)
    
    #Sets to other dirs
    elif wallpaperchoice == "halloween":
        #wallpaperchoice = os.popen(f'ls {hallo}/ | dmenu -p "Wallpaper:"').read().strip('\n')
        wallpaperchoice = dmenu(os.popen(f"ls {hallo}").read().strip('\n'), "Wallpaper:")        

        if wallpaperchoice == '':
            sys.exit(0)
    
        curr_dir = hallo

    elif wallpaperchoice == "xmas":
        #wallpaperchoice = os.popen(f'ls {xmas}/ | dmenu -p "Wallpaper:"').read().strip('\n')
        wallpaperchoice = dmenu(os.popen(f"ls {xmas}").read().strip('\n'), "Wallpaper:")        
        
        if wallpaperchoice == '':
            sys.exit(0)
    
        curr_dir = xmas

    elif wallpaperchoice == "All":
        #wallpaperchoice = os.popen(f'ls {stdwalls}/ | dmenu -p "Wallpaper:"').read().strip('\n')
        wallpaperchoice = dmenu(os.popen(f"ls {stdwalls}").read().strip('\n'), "Wallpaper:")        
        
        if wallpaperchoice == '':
            sys.exit(0)
    
        curr_dir = stdwalls

    else:
        #wallpaperchoice = os.popen(f'ls {home}/Pictures/Wallpapers/wallpaperdirs/{wallpaperchoice} | dmenu -p "Wallpaper:"').read().strip('\n')
        wallpaperchoice = dmenu(os.popen(f"ls {home}/Pictures/Wallpapers/wallpaperdirs/{wallpaperchoice}").read().strip('\n'), "Wallpaper:")        

        if wallpaperchoice == '':
            sys.exit(0)
    
        curr_dir = stdwalls

    #Split to list 
    wallargs = wallpaperchoice.split(' ')
    
    #Handle random selection
    if 'random' in wallargs:
        wallpapers = os.popen(f'ls {home}/Pictures/Wallpapers/{curr_dir}').read().strip('\n').split('\n')
    
        wallargs[0] = random(wallpapers)

    #Set wallpaper with nitrogen
    if ':nitro' in wallargs:
        nitro(wallargs[0], curr_dir)
        sys.exit(0)

    elif preset(wallargs[0], 'find') and ':auto' not in wallargs:
        os.system(f'wal --theme {home}/Pictures/Wallpapers/presets/{preset(wallargs[0], "set")}')

        run([f'{home}/.local/bin/pywalfox', 'update'])

    #Set wallpaper with pywal
    else:
        pywal(wallargs[0], curr_dir)
        run([f'{home}/.local/bin/pywalfox', 'update'])

    with open(f"{home}/.cache/wal/colors.json", 'r') as f:
        data = json.load(f)

        top_color = data["colors"].get('color8')

        bottom_color = data["colors"].get('color6')

    gen_pylogo(top_color, bottom_color)

    try:
        print('restarting qtile')
        #subprocess.check_call(['qtile', 'shell', '-c', 'restart()'])
        #run(['qtile', 'shell', '-c', 'restart()'])
        #run(['kill', '-SIGUSR1', os.popen('pidof qtile').read().strip('\n')])
        run([f'{home}/git/qtile/bin/qtile', 'shell', '-c', 'reload_config()'])
        #os.system('qtile shell -c "restart()"')

    except:
        print('killing dwm')
        os.system('killall dwm')
        #pass
