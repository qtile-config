## Mike's Custom Qtile Config

### Features
---
- Pywal Integration.
- Window Swallowing.
- Cryptocurrency Widget.

Automatic reloads from wallpaper.py and colors.py assumes that the `qtile` script is located in `$HOME/git/qtile/bin/`. I use the master branch of qtile from github.

### Dependencies
---
- [Mpd2](https://pypi.org/project/python-mpd2/). Host is ~/.mpd/socket. Port is 6600.
- [Ario](https://ario-player.sourceforge.net/).
- Imagemagick.
- [Pywal.](https://github.com/dylanaraps/pywal)
- [Xsnow.](https://www.ratrabbit.nl/ratrabbit/xsnow/)
- Xautolock. Uses [slock](https://tools.suckless.org/slock/) with xres and message patches. I might put my fork up in the future.
- [Dmenu.](https://github.com/LukeSmithxyz/dmenu)
- [Flameshot.](https://flameshot.org/)
- Powerline Symbols.

### Defaults
---
Default programs are located at the top of `config.py`.

- Terminal Emulator: [Alacritty.](https://github.com/alacritty/alacritty)
- Browser: [Librewolf.](https://librewolf.net/)

### Key Bindings
---
|Binding|Action|
|-|-|
|MODKEY + RETURN|Opens Terminal|
|MODKEY + [1-10]|Go to group.|
|MODKEY + SHIFT + [1-10]|Send window to group.|
|MODKEY + CONTROL + [1-10]|Send window to group and follow.|
|MODKEY + H|Switch to window on the left|
|MODKEY + L|Switch to window on the right|
|MODKEY + K|Switch to window above.|
|MODKEY + J|Switch to window below.|
|MODKEY + SHIFT + H|Move window to the left|
|MODKEY + SHIFT + L|Move window to the right|
|MODKEY + SHIFT + K|Move window up.|
|MODKEY + SHIFT + J|Move window down.|
|MODKEY + SHIFT + SPACE|Flip windows.|
|MODKEY +CONTROL + K|Grow window.|
|MODKEY + CONTROL + J|Shrink window.|
|MODKEY + ALT + K|Move to next group.|
|MODKEY + ALT + J|Move to previous group.|
|MODKEY + M|Mute/Unmute microphone.|
|MODKEY + ,|Mpd: Go to previous song.|
|MODKEY + SHIFT + ,|Mpd: Seek to begining of song.|
|MODKEY + .|Mpd: Play next song.|
|MODKEY + SHIFT + .|Mpd: Toggle repeat.|
|MODKEY + SHIFT + P|Mpd: Play/Pause song.|
|MODKEY + [|Mpd: Seek backward 10 seconds.|
|MODKEY + ]|Mpd: Seek forward 10 seconds.|
|MODKEY + SHIFT + [|Mpd: Seek backward 60 seconds.|
|MODKEY + SHIFT + ]|Mpd: Seek forward 60 seconds.|
|MODKEY + CONTROL + [|Mpd: Lower volume by 5%.|
|MODKEY + CONTROL + ]|Mpd: Raise volume by 5%.|
|MODKEY + TAB|Next layout.|
|MODKEY + SHIFT + F|Toggle floating window.|
|MODKEY + SPACE|Toggle fullscreen window.|
|MODKEY + W|Kill window.|
|MODKEY + CONTROL + R|Restart Qtile.|
|MODKEY + CONTROL + Q|Close Qtile.|
|MODKEY + B|Hide status bar.|
|MODKEY + C|Opens Xterm|
|MODKEY + P|Dmenu run.|
|MODKEY + R|Scratchpad.|
|MODKEY + CONTROL + P|Ario scratchpad.|
|MODKEY + I|Run an Appimage.|
|MODKEY + F|Open web browser.|
|MODKEY + CONTROL + F|Open private web browser.|
|MODKEY + SHIFT + S|Open flameshot.|
|MODKEY + CONTROL + C|Open colors menu.|
|MODKEY + CONTROL + L|Xautolock: lock now.|
|MODKEY + ALT + L|Xautolock: toggle.|
|MODKEY + CONTROL + N|Open wallpaper menu.|
|MODKEY + Z|Reload config.|
|MODKEY + CONTROL + Z|Manually unswallow window.|
|MODKEY + SHIFT + Z|Manually swallow window.|
|MODKEY + N|Pin window.|
|MODKEY + SHIFT + N|Unpin window.|
|MODKEY + U|Go to 'swallow' group.|
|MODKEY + SHIFT + U|Send window to 'swallow' group.|
|MODKEY + CONTROL + U|Send window to 'swallow' group and follow.|
|MODKEY + Y|Go to 'hidden' group.|
|MODKEY + SHIFT + Y|Send window to 'hidden' group.|
|MODKEY + CONTROL + Y|Send window to 'hidden' group and follow.|
|MODKEY + Q|Display clipboard contents as a QR code.|
|MODKEY + CONTROL + E|Open the hammer editor.|

### Copyright
---
Copyright (c) 2012-2015 Tycho Andersen,
Copyright (c) 2013 xarvh,
Copyright (c) 2013 horsik,
Copyright (c) 2013-2014 roger,
Copyright (c) 2013 Tao Sauvage,
Copyright (c) 2014 ramnes,
Copyright (c) 2014 Sean Vig,
Copyright (c) 2014 Adi Sieker.

Modified by Michael Kantor

Permission is hereby granted, free of charge, to any person obtaining a copy of this
  software and associated documentation files (the “Software”), to deal in the Software
  without restriction, including without limitation the rights to use, copy, modify,
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to the following
  conditions:
The above copyright notice and this permission notice shall be included in all copies
  or substantial portions of the Software.

## Custom Modules

### Appimage
---
`appimg.py`

Opens a dmenu that runs Appimages. Appimages must be located in the `Applications` directory in your home directory.

### Colors
---
`colors.py`

Opens a dmenu prompt that loads a pywal `json` file. Files must be located in the `colors` directory in your `qtile` config folder.

### Holiday
----
`holiday.py`

Tells `wallpaper.py` what holiday it is. Halloween, Thanksgiving, and Christmas are supported.

### Wallpaper
---
`wallpaper.py`

Sets the wallpaper using pywal. Wallpapers can be set with nitrogen by adding ":nitro" behind the filename. Wallpapers must be located in `~/Pictures/Wallpapers/wallpapers/`. Halloween wallpapers must be located in `~/Pictures/Wallpapers/halloweenwallpapers/`. Christmas wallpapers must be located in `~/Pictures/Wallpapers/xmaswallpapers/`. The first prompt will be "All" and selecting it will show all wallpapers. Additional groups can be added by creating a folder in `~/Pictures/Wallpapers/wallpaperdirs/`. To add wallpapers to a group, add the wallpaper to the main wallpapers directory than touch a file or create a symbolic link under the group's folder with the same name. The wallpaper must be in the main wallpapers directory.

Presets can be created by adding a pywal `json` file to `~/Pictures/presets/`. Presets must have the same name as the wallpaper sans the file extension. For example, a preset for `~/Pictures/Wallpapers/wallpapers/nature.jpg` would be `~/Pictures/Wallpapers/presets/nature.json`. The `json` files generated by pywal can be found in `~/.cache/wal/colors.json`. Presets can be overriden by adding ":auto" after the filename.

To set a wallpaper from the shell, add "-i" and then the file path.
\ No newline at end of file

## Custom Widgets
### Ticker Gecko
---
`ticker_gecko.py`

Gets the price of a cryptocurrency from [CoinGecko](https://www.coingecko.com/). Add the ticker to the option in `autostart.sh`. The free CoinGecko api allows for 30 calls/min. The daemon will calculate how much time to wait between calls.

### Supported Currencies
---
- BTC
- ETH
- XRP
- LTC
- BCH
- XMR
- DOGE

More can be added to the "currencies" variable in the "match" function.
